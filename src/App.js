import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import InputPage from "./pages/InputPage";
import OutputPage from "./pages/OutputPage";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={InputPage} />
        <Route path="/output" component={OutputPage} />
      </Switch>
    </Router>
  );
};

export default App;
