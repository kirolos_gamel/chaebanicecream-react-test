import React, { useState } from "react";
import { Form, Container, Row, Col, Button } from "react-bootstrap";
import "./InputPage.css";

const InputPage = (props) => {
  const [date, setDate] = useState("");
  const [message, setMessage] = useState("");
  const [selectedTheme, setSelectedTheme] = useState("");
  const availableThemes = [
    { label: "Dark Theme (Default)", value: "dark" },
    { label: "Light Theme", value: "light" },
    { label: "Minimal Light Theme", value: "minimal" },
  ];

  const generateOutput = (e) => {
    e.preventDefault();

    sessionStorage.setItem(
      "formData",
      JSON.stringify({
        date,
        message,
        selectedTheme,
      })
    );

    props.history.push("/output");
  };

  return (
    <Container fluid className="input">
      <Form>
        <Row className="input__row--1">
          <Col md>
            <Form.Group controlId="userDate">
              <Form.Label>Date :</Form.Label>
              <Form.Control
                type="date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="userMessage">
              <Form.Label>Message :</Form.Label>
              <Form.Control
                as="textarea"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                aria-label="Message"
                placeholder="Message goes here..."
                spellCheck={true}
              />
            </Form.Group>
          </Col>
          <Col md>
            <h3 className="mb-3">Select a Theme :</h3>
            {availableThemes.map(({ label, value }, index) => (
              <Form.Group key={value} controlId={`userTheme-${index++}`}>
                <Form.Check
                  type="radio"
                  name="output-theme"
                  value={value}
                  id={`theme--${index++}`}
                  label={label}
                  onChange={(e) => setSelectedTheme(e.target.value)}
                />
              </Form.Group>
            ))}
          </Col>
        </Row>

        <Row className="justify-content-md-center">
          <Col>
            <Button
              onClick={generateOutput}
              disabled={!date || !message || !selectedTheme}
              type="submit"
            >
              Generate Output
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};

export default InputPage;
