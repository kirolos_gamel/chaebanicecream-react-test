import React, { useEffect } from "react";
import { Button, Container } from "react-bootstrap";
import "./OutputPage.css";
// import { AiTwotoneCalendar } from "react-icons/ai";
import { toggleTheme } from "../utils";
import { Link } from "react-router-dom";

const OutputPage = () => {
  const formData = sessionStorage.getItem("formData");
  const { date, message, selectedTheme } = JSON.parse(formData);

  useEffect(() => {
    toggleTheme(selectedTheme);
  }, [selectedTheme]);

  return (
    <Container fluid className="output">
      <div className="content">
        <h2>
          {date}
        </h2>
        <p><b>Message:</b> {message}</p>
        <Link to="/">
          <Button className="output__btn">&larr; Back to Input</Button>
        </Link>
      </div>
    </Container>
  );
};

export default OutputPage;
