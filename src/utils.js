export const toggleTheme = (selectedTheme) => {
  const HTMLDocument = document.getElementsByTagName("html")[0];

  if (selectedTheme === "dark") {
    return HTMLDocument.removeAttribute("data-theme");
  }

  HTMLDocument.setAttribute("data-theme", selectedTheme);
};
